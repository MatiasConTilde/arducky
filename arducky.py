import sys, os

keys_map = {'ALT': 'KEY_LEFT_ALT',
            'APP': '229',
            'CAPSLOCK': 'KEY_CAPS_LOCK',
            'CMD': 'KEY_LEFT_GUI',
            'COMMAND': 'KEY_LEFT_GUI',
            'CONTROL': 'KEY_LEFT_CTRL',
            'CTRL': 'KEY_LEFT_CTRL',
            'DEL': 'KEY_DELETE',
            'DELETE': 'KEY_DELETE',
            'DOWN': 'KEY_DOWN_ARROW',
            'DOWNARROW': 'KEY_DOWN_ARROW',
            'END': 'KEY_END',
            'ENTER': 'KEY_RETURN',
            'ESC': 'KEY_LEFT_ESC',
            'ESCAPE': 'KEY_LEFT_ESC',
            'F1': 'KEY_F1',
            'F10': 'KEY_F10',
            'F11': 'KEY_F11',
            'F12': 'KEY_F12',
            'F2': 'KEY_F2',
            'F3': 'KEY_F3',
            'F4': 'KEY_F4',
            'F5': 'KEY_F5',
            'F6': 'KEY_F6',
            'F7': 'KEY_F7',
            'F8': 'KEY_F8',
            'F9': 'KEY_F9',
            'GUI': 'KEY_LEFT_GUI',
            'HOME': 'KEY_HOME',
            'LALT': 'KEY_LEFT_ALT',
            'LCOMMAND': 'KEY_LEFT_GUI',
            'LCTRL': 'KEY_LEFT_CTRL',
            'LEFT': 'KEY_LEFT_ARROW',
            'LEFTARROW': 'KEY_LEFT_ARROW',
            'LGUI': 'KEY_LEFT_GUI',
            'LSHIFT': 'KEY_LEFT_SHIFT',
            'LWINDOWS': 'KEY_LEFT_GUI',
            'MENU': '229',
            'PAGEDOWN': 'KEY_PAGE_DOWN',
            'PAGEUP': 'KEY_PAGE_UP',
            'PRINTSCREEN': '206',
            'RALT': 'KEY_RIGHT_ALT',
            'RCMD': 'KEY_RIGHT_GUI',
            'RCOMMAND': 'KEY_RIGHT_GUI',
            'RCTRL': 'KEY_RIGHT_CTRL',
            'RETURN': 'KEY_RETURN',
            'RGUI': 'KEY_RIGHT_GUI',
            'RIGHT': 'KEY_RIGHT_ARROW',
            'RIGHTARROW': 'KEY_RIGHT_ARROW',
            'RSHIFT': 'KEY_RIGHT_SHIFT',
            'RWINDOWS': 'KEY_RIGHT_GUI',
            'SHIFT': 'KEY_LEFT_SHIFT',
            'SPACE': '\' \'',
            'TAB': 'KEY_TAB',
            'UP': 'KEY_UP_ARROW',
            'UPARROW': 'KEY_UP_ARROW',
            'WINDOWS': 'KEY_LEFT_GUI',
            '0': '\'0\'',
            '1': '\'1\'',
            '2': '\'2\'',
            '3': '\'3\'',
            '4': '\'4\'',
            '5': '\'5\'',
            '6': '\'6\'',
            '7': '\'7\'',
            '8': '\'8\'',
            '9': '\'9\'',
            'a': '\'a\'',
            'b': '\'b\'',
            'c': '\'c\'',
            'd': '\'d\'',
            'e': '\'e\'',
            'f': '\'f\'',
            'g': '\'g\'',
            'h': '\'h\'',
            'i': '\'i\'',
            'j': '\'j\'',
            'k': '\'k\'',
            'l': '\'l\'',
            'm': '\'m\'',
            'n': '\'n\'',
            'o': '\'o\'',
            'p': '\'p\'',
            'q': '\'q\'',
            'r': '\'r\'',
            's': '\'s\'',
            't': '\'t\'',
            'u': '\'u\'',
            'v': '\'v\'',
            'w': '\'w\'',
            'x': '\'x\'',
            'y': '\'y\'',
            'z': '\'z\'',
            'A': '\'A\'',
            'B': '\'B\'',
            'C': '\'C\'',
            'D': '\'D\'',
            'E': '\'E\'',
            'F': '\'F\'',
            'G': '\'G\'',
            'H': '\'H\'',
            'I': '\'I\'',
            'J': '\'J\'',
            'K': '\'K\'',
            'L': '\'L\'',
            'M': '\'M\'',
            'N': '\'N\'',
            'O': '\'O\'',
            'P': '\'P\'',
            'Q': '\'Q\'',
            'R': '\'R\'',
            'S': '\'S\'',
            'T': '\'T\'',
            'U': '\'U\'',
            'V': '\'V\'',
            'W': '\'W\'',
            'X': '\'X\'',
            'Y': '\'Y\'',
            'Z': '\'Z\''}

if len(sys.argv) == 2:
    in_file = open(sys.argv[1], 'r')
    os.makedirs('output', exist_ok=True)
    out_file = open('output/output.ino', 'w+')
elif len(sys.argv) == 3:
    in_file = open(sys.argv[1], 'r')
    os.makedirs(sys.argv[2], exist_ok=True)
    out_file = open(f'{sys.argv[2]}/{sys.argv[2]}.ino', 'w+')
else:
    print('Error: must declare input file')
    exit()

in_lines = in_file.readlines()
in_file.close()

def uses(command):
    return any(command in line for line in in_lines)

out_file.write('#include <Keyboard.h>\n')
if uses('MOUSE'):
    out_file.write('#include <Mouse.h>\n')
if uses('SWITCH'):
    out_file.write('int whichScript = 0;\n')
out_file.write('void typeKey(int key) {\n')
out_file.write('Keyboard.press(key);\n')
out_file.write('delay(50);\n')
out_file.write('Keyboard.release(key);\n')
out_file.write('}\n')
out_file.write('void setup() {\n')
out_file.write('Keyboard.begin();\n')
if uses('MOUSE'):
    out_file.write('Mouse.begin();\n')
if uses('SWITCH'):
    out_file.write('pinMode(2, INPUT_PULLUP);\n')
    out_file.write('pinMode(3, INPUT_PULLUP);\n')
    out_file.write('pinMode(4, INPUT_PULLUP);\n')
    out_file.write('pinMode(5, INPUT_PULLUP);\n')
    out_file.write('if (!digitalRead(2)) whichScript += 1;\n')
    out_file.write('if (!digitalRead(3)) whichScript += 2;\n')
    out_file.write('if (!digitalRead(4)) whichScript += 4;\n')
    out_file.write('if (!digitalRead(5)) whichScript += 8;\n')
out_file.write('delay(500);\n')
if uses('SWITCH'):
    out_file.write('switch(whichScript) {\n')

in_loop = False
first_case = True
default_delay = 0

for line in in_lines:
    # Remove leading and trailing whitespace
    line = line.strip()
    words = line.split(' ')
    command = words[0]
    # Everything except command
    remaining = line[line.find(' ')+1:]

    if command in ['DEFAULTDELAY', 'DEFAULT_DELAY']:
        default_delay = int(remaining)
        continue

    if command == 'REM':
        # Comment
        out_file.write(f'// {remaining}\n')
        continue

    if command == 'SWITCH':
        if not first_case:
            out_file.write('break;\n')
        out_file.write(f'case {words[1]}:\n')
        first_case = False
        continue

    if command == 'LOOP' and not in_loop:
        # Enter loop() {}
        if uses('SWITCH'):
            out_file.write('default:\n')
            out_file.write('break;\n')
            out_file.write('}\n')
        out_file.write('}\n')
        out_file.write('void loop() {\n')
        if uses('SWITCH'):
            out_file.write('switch(whichScript) {\n')
        in_loop = True
        first_case = True
        continue

    if command == 'MOUSE':
        if words[1] == 'MOVE':
            out_file.write(f'Mouse.move({words[2]}, {words[3]});\n')
        else:
            # Click, press or release
            mouse_command = words[1].lower()
            button = f'MOUSE_{words[2]}'.upper() if len(words) > 2 else ''
            out_file.write(f'Mouse.{mouse_command}({button});\n')

    elif line in keys_map:
        # Type single key
        out_file.write(f'typeKey({keys_map[line]});\n')

    elif command == 'STRING':
        # Write text
        remaining = remaining.replace('\\', '\\\\')
        remaining = remaining.replace('"', '\\"')
        out_file.write(f'Keyboard.print("{remaining}");\n')

    elif command == 'DELAY':
        out_file.write(f'delay({remaining});\n')

    elif command in keys_map:
        # Press multiple keys at once
        while len(line) > 0:
            space_index = line.find(' ')
            if space_index < 0:
                # If it's the last key
                space_index = len(line)
            key = line[:space_index]
            out_file.write(f'Keyboard.press({keys_map[key]});\n')
            # Trim line to remove already used key
            line = line[space_index + 1:]
        out_file.write('delay(50);\n')
        out_file.write('Keyboard.releaseAll();\n')

    else:
        # No duckyscript command (probably inline Arduino code)
        out_file.write(line + '\n')

    if default_delay > 0 and not command in ['REM', 'DEFAULTDELAY', 'DEFAULT_DELAY']:
        # Apply the default delay
        out_file.write(f'delay({default_delay});\n')

    if uses('SWITCH') and in_loop and first_case and not line == '':
        print('Error: must declare switch position after using loop')
        exit()

# Close switch() {}
if uses('SWITCH'):
    out_file.write('break;\n')
    out_file.write('default:\n')
    out_file.write('break;\n')
    out_file.write('}\n')

# Close loop() {}
out_file.write('}\n')
if not in_loop:
    out_file.write('\nvoid loop() {}\n')

out_file.close()
