# arducky

Parser that reads duckyscript from a file and generates an Arduino sketch for use with an Arduino Leonardo or similar

## How to use:

Execute the file with `python arducky.py input.txt`

You can change the output file by adding args, for example: `python arducky.py custom_file.txt project_name`

If not specified, `output/output.ino` will be used

See [`input.txt`](input.txt) for an example file and documentation

## Features

- Fully compatible with the [original duckyscript definition](https://github.com/hak5darren/USB-Rubber-Ducky/wiki/Duckyscript) by Hak5 plus some extra additions
- Works offline and creates an Arduino file directly, not a text you have to copy-paste
- Multiple scripts at once (see [Multiscript](#multiscript))
- You can write Arduino code inline and it will get copied directly, allowing much greater control and implementing real logic in the scripts
- Added commands:
    - `LOOP` will put all following code in `void loop() {}` so it keeps repeating forever
    - `MOUSE` can move the mouse and click

## Multiscript

This feature allows to save multiple scripts at once and execute each one according to a dipswitch position.

To use this feature, you have to solder a dipswitch on the arduino on pins 2-5. To select a script, set each switch on or off to make the binary representation of the scripts number, for example ON-OFF-ON-ON for 11.

In the input file, write `SWITCH n` (replacing n by the scripts number) before the actual commands of that scripts. You have to select the switch position again after `LOOP`.

An example of this is at [`switches.txt`](switches.txt)

Inspired by [Seytonic](https://www.youtube.com/watch?v=_yJWwKO3_Z0) and [Duckuino](https://github.com/Nurrl/Duckuino)
